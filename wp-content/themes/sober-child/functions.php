<?php
/**
 * Sober functions and definitions.
 *
 * @link    https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Sober Child
 */

add_action( 'wp_enqueue_scripts', 'sober_child_enqueue_scripts', 20 );

function sober_child_enqueue_scripts() {
	wp_enqueue_style( 'sober-child', get_stylesheet_uri() );
}


add_image_size( 'thumb-420x320', 420, 320 , true); 
add_image_size( 'thumb-640x420', 640, 420 , true); 

function arphabet_widgets_init() {

	register_sidebar( array(
		'name'          => 'footer 1',
		'id'            => 'footer-1',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget_footer_title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => 'footer 2',
		'id'            => 'footer-2',
		'before_widget' => '<div class="last_posts_footer">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget_footer_title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => 'footer 3',
		'id'            => 'footer-3',
		'before_widget' => '<div class="footer_tag">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget_footer_title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => 'wyszukiwarka',
		'id'            => 'search',
		'before_widget' => '<section class="widget widget_search">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'arphabet_widgets_init' );


function button_matrix_func( $atts ) {
	$atts = shortcode_atts( array(
		'title' => 'powinieneś uzupełnić tytuł',
		'url' => 'brak linku url'
	), $atts, 'button' );
	$button = "";
	$button .= '<p style="text-align:center;">';
	$button .= '<a class="btn_matrix" href="'.$atts['url'].'" target="_blank">';
	$button .= $atts['title'];
	$button .= '</a>';
	$button .= '</p>';
	return $button;
}
add_shortcode( 'button', 'button_matrix_func' );



if ( ! function_exists( 'sober_entry_thumbnail' ) ) :
	/**
	 * Show entry thumbnail base on its format
	 *
	 * @since  1.0
	 */
	function sober_entry_thumbnail( $size = 'thumbnail' ) {
		$html = '';

		switch ( get_post_format() ) {
			case 'gallery':
				$images = get_post_meta( get_the_ID(), 'images' );

				if ( empty( $images ) ) {
					break;
				}

				$gallery = array();
				foreach ( $images as $image ) {
					$gallery[] = wp_get_attachment_image( $image, $size );
				}
				$html .= '<div class="entry-gallery entry-image">' . implode( '', $gallery ) . '</div>';
				break;

			case 'audio':

				$thumb = get_the_post_thumbnail( get_the_ID(), $size );
				if ( ! empty( $thumb ) ) {
					$html .= '<a class="entry-image" href="' . get_permalink() . '">' . $thumb . '</a>';
				}

				$audio = get_post_meta( get_the_ID(), 'audio', true );
				if ( ! $audio ) {
					break;
				}

				// If URL: show oEmbed HTML or jPlayer
				if ( filter_var( $audio, FILTER_VALIDATE_URL ) ) {
					if ( $oembed = @wp_oembed_get( $audio, array( 'width' => 1140 ) ) ) {
						$html .= $oembed;
					} else {
						$html .= '<div class="audio-player">' . wp_audio_shortcode( array( 'src' => $audio ) ) . '</div>';
					}
				} else {
					$html .= $audio;
				}
				break;

			case 'video':
				$video = get_post_meta( get_the_ID(), 'video', true );
				if ( ! $video ) {
					break;
				}

				// If URL: show oEmbed HTML
				if ( filter_var( $video, FILTER_VALIDATE_URL ) ) {
					if ( $oembed = @wp_oembed_get( $video, array( 'width' => 1140 ) ) ) {
						$html .= $oembed;
					} else {
						$atts = array(
							'src'   => $video,
							'width' => 1140,
						);

						if ( has_post_thumbnail() ) {
							$atts['poster'] = get_the_post_thumbnail_url( get_the_ID(), 'full' );
						}
						$html .= wp_video_shortcode( $atts );
					}
				} // If embed code: just display
				else {
					$html .= $video;
				}
				break;

			default:
				$alt = get_post_meta(get_post_thumbnail_id($post_id), '_wp_attachment_image_alt', true) ;
				$title = get_the_title(get_post_thumbnail_id($post_id)); 
				 if(!$alt): $arr = array('alt' => $title); else: $arr = array('alt' => $alt);endif;
				$html = get_the_post_thumbnail( get_the_ID(), $size , $arr);
//				var_dump($title);
				break;
		}

		echo apply_filters( __FUNCTION__, $html, get_post_format() );
	}

endif;
?>


