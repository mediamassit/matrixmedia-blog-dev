<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Sober
 */
?>
	

		<?php do_action( 'sober_after_content' ); ?>
	</div><!-- #content -->
	<div class="footer_custom">
		<div class="container">
			<div class="row">
				<div class="footer_wrapper">
					<div class="col-md-4">
						<?php if ( is_active_sidebar( 'footer-1' ) ) : ?>
							<div id="primary-sidebar" class="widget widget_categories footer_categories" role="complementary">
								<?php dynamic_sidebar( 'footer-1' ); ?>
							</div><!-- #primary-sidebar -->
						<?php endif; ?>
					</div>
					<div class="col-md-4">
						<?php if ( is_active_sidebar( 'footer-2' ) ) : ?>
							<div id="primary-sidebar" class="widget widget_categories footer_categories" role="complementary">
								<?php dynamic_sidebar( 'footer-2' ); ?>
							</div><!-- #primary-sidebar -->
						<?php endif; ?>
					</div>
					<div class="col-md-4">
						<?php if ( is_active_sidebar( 'footer-3' ) ) : ?>
							<div id="primary-sidebar" class="widget widget_categories footer_categories" role="complementary">
								<?php dynamic_sidebar( 'footer-3' ); ?>
							</div><!-- #primary-sidebar -->
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="footer-info">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<?php $fb = get_field("facebook",option);
						if($fb): ;?>
							<a href="<?php echo $fb;?>" target="_blank">
								<i class="fa fa-facebook" aria-hidden="true" style="font-size:22px;color:#fff;margin-top:3px;"></i>
							</a>
						<?php endif;?>
					</div>
					<div class="col-md-6">
						<div class="right_copyright">
							<?php $copy = get_field("copyright", option) ;
								echo $copy
							;?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<?php  do_action( 'sober_before_footer' ) ?>
	<?php /*
	<footer id="colophon" class="site-footer" role="contentinfo">
		<?php do_action( 'sober_footer' ) ?>
	</footer><!-- #colophon --><?php */ ;?>

	<?php do_action( 'sober_after_footer' ) ?>
</div><!-- #page -->

<?php do_action( 'sober_after_site' ) ?>

<?php wp_footer(); ?>
<script src="<?php echo get_stylesheet_directory_uri();?>/OwlCarousel2/dist/owl.carousel.min.js"></script>

<script type="text/javascript">
jQuery(function($) { // DOM is now ready and jQuery's $ alias sandboxed

   $('.owl-carousel').owlCarousel({
    loop:true,
    nav:false,
	autoplay: true,
	dots:false,
	smartSpeed:2000,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:3
        }
    }
   });	
});
</script>
<script type="text/javascript">

    /** This section is only needed once per page if manually copying **/
    if (typeof MauticSDKLoaded == 'undefined') {
        var MauticSDKLoaded = true;
        var head            = document.getElementsByTagName('head')[0];
        var script          = document.createElement('script');
        script.type         = 'text/javascript';
        script.src          = 'http://mailing.mediamass.pl/media/js/mautic-form.js';
        script.onload       = function() {
            MauticSDK.onLoad();
        };
        head.appendChild(script);
        var MauticDomain = 'http://mailing.mediamass.pl';
        var MauticLang   = {
            'submittingMessage': "Proszę czekać..."
        }
    }
</script>
</body>
</html>
