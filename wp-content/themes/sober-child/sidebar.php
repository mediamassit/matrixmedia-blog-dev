<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link    https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Sober
 */


if ( 'no-sidebar' == sober_get_layout() ) {
	return;
}

$sidebar         = 'blog-sidebar';
$sidebar_classes = array( 'col-md-4' );

if ( function_exists( 'is_woocommerce' ) && is_woocommerce() ) {
	$sidebar = 'shop-sidebar';
} elseif ( is_page() ) {
	$sidebar         = 'page-sidebar';
	$sidebar_classes = array( 'col-md-3' );
}

$sidebar_classes[] = $sidebar;
?>

<aside id="secondary" class="widget-area primary-sidebar <?php echo esc_attr( join( ' ', $sidebar_classes ) ) ?> " role="complementary">
	<?php 
	 if ( is_active_sidebar( 'search' ) ) :
		 dynamic_sidebar( 'search' );?>
		<?php $fb = get_field("facebook",option);
		if($fb): ?>
		<div class="social_box">
		<a href="<?php echo $fb ;?>" target="_blank"><div class="social_custom"><h2>Znajdź nas na facebooku <i class="fa fa-facebook" aria-hidden="true" style="font-size:22px;color:#fff;float:right;"></i></h2></div></a>
		</div>
		<?php endif ;?>
		<section class="widget">
<!--
			<h2 class="widget-title widget_title_newsletter">NEWSLETTER</h2>
			<input id="mauticform_input_marixblog_email" name="mauticform[email]" class="newsletter_widget" type="e-mail" placeholder=" Podaj wsój adres e-mail...">
			<input type="submit" id="mauticform_input_marixblog_submit" name="mauticform[submit]" class="newsletter-submit " value="1">
-->
		
<style type="text/css" scoped>
    .mauticform_wrapper { max-width: 600px; margin: 10px auto; }
    .mauticform-innerform {}
    .mauticform-post-success {}
    .mauticform-name { font-weight: bold; font-size: 1.5em; margin-bottom: 3px; }
    .mauticform-description { margin-top: 2px; margin-bottom: 10px; }
    .mauticform-error { margin-bottom: 10px; color: red; }
    .mauticform-message { margin-bottom: 10px;color: green; }
    .mauticform-row { display: inline !important; margin-bottom: 20px; }
    .mauticform-label { font-size: 1.1em; display: block; font-weight: bold; margin-bottom: 5px; }
    .mauticform-row.mauticform-required .mauticform-label:after { color: #e32; content: " *"; display: inline; }
    .mauticform-helpmessage { display: block; font-size: 0.9em; margin-bottom: 3px; }
    .mauticform-errormsg { display: block; color: red; margin-top: 2px; }
    .mauticform-selectbox, .mauticform-input, .mauticform-textarea { width: 100%; padding: 0.5em 0.5em; border: 1px solid #CCC; background: #fff; box-shadow: 0px 0px 0px #fff inset; border-radius: 4px; box-sizing: border-box; }
    .mauticform-checkboxgrp-row {}
    .mauticform-checkboxgrp-label { font-weight: normal; }
    .mauticform-checkboxgrp-checkbox {}
    .mauticform-radiogrp-row {}
    .mauticform-radiogrp-label { font-weight: normal; }
    .mauticform-radiogrp-radio {}
    .mauticform-button-wrapper .mauticform-button.btn-default, .mauticform-pagebreak-wrapper .mauticform-pagebreak.btn-default { color: #5d6c7c;background-color: #ffffff;border-color: #dddddd;}
    .mauticform-button-wrapper .mauticform-button, .mauticform-pagebreak-wrapper .mauticform-pagebreak { display: inline-block;margin-bottom: 0;font-weight: 600;text-align: center;vertical-align: middle;cursor: pointer;background-image: none;border: 1px solid transparent;white-space: nowrap;padding: 6px 12px;font-size: 13px;line-height: 1.3856;border-radius: 3px;-webkit-user-select: none;-moz-user-select: none;-ms-user-select: none;user-select: none;}
    .mauticform-button-wrapper .mauticform-button.btn-default[disabled], .mauticform-pagebreak-wrapper .mauticform-pagebreak.btn-default[disabled] { background-color: #ffffff; border-color: #dddddd; opacity: 0.75; cursor: not-allowed; }
    .mauticform-pagebreak-wrapper .mauticform-button-wrapper {  display: inline; }
</style>
<div id="mauticform_wrapper_marixblog" class="mauticform_wrapper">
    <form autocomplete="false" role="form" method="post" action="http://mailing.mediamass.pl/form/submit?formId=1" id="mauticform_marixblog" data-mautic-form="marixblog">
        <div class="mauticform-error" id="mauticform_marixblog_error"></div>
        <div class="mauticform-message" id="mauticform_marixblog_message"></div>
        <div class="mauticform-innerform">

            
          <div class="mauticform-page-wrapper mauticform-page-1" data-mautic-form-page="1">

            <div id="mauticform_marixblog_email"  class="mauticform-row mauticform-email mauticform-field-1">
                <input id="mauticform_input_marixblog_email" name="mauticform[email]" class="newsletter_widget" type="e-mail" placeholder=" Podaj swój adres e-mail...">
                <span class="mauticform-errormsg" style="display: none;"></span>
            </div>

            <div id="mauticform_marixblog_submit"  class="mauticform-row mauticform-button-wrapper mauticform-field-2">
               <input type="submit" id="mauticform_input_marixblog_submit" name="mauticform[submit]" class="newsletter-submit " value="1">
            </div>
            </div>
        </div>

        <input type="hidden" name="mauticform[formId]" id="mauticform_marixblog_id" value="1"/>
        <input type="hidden" name="mauticform[return]" id="mauticform_marixblog_return" value=""/>
        <input type="hidden" name="mauticform[formName]" id="mauticform_marixblog_name" value="marixblog"/>
</form>
</div>

		</section>	 
 <?php endif;
	;?>
	<?php dynamic_sidebar( $sidebar ); ?>



</aside><!-- #secondary -->



