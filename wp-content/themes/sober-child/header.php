<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Sober
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta property="og:locale" content="pl_PL" />
<meta property="og:type" content="website" />
<meta property="og:title" content="BLOG MatrixMedia.pl " />
<meta name="description" content="BLOG MatrixMedia.pl - Blog poświęcony nowinkom z rynków AGD i RTV - prowadzony przez specjalistów z MatrixMedia.pl">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;subset=latin-ext" rel="stylesheet">
<link rel="icon" href="<?php echo get_stylesheet_directory_uri() ;?>/logo.png" type="image/gif" sizes="16x16">
<?php wp_head(); 
	$args = array(
		'post_type' => 'post',
		'posts_per_page'   => -1,
	);
$slider_query = new WP_Query($args);
?>
<meta name="google-site-verification" content="jM1sP_mYnuvF-oAeMvQx0PrWjTZFF7EWicFHxA3NKY4" />
</head>

<body <?php body_class(); ?>>

<?php do_action( 'sober_before_site' ) ?>

<div id="page" class="site">
	<?php do_action( 'sober_before_header' ); ?>

	<header id="masthead" class="site-header" role="banner">
		<div class="container clearfix">

			<?php do_action( 'sober_header' ); ?>

		</div>
	</header><!-- #masthead -->
		<?php if(!is_404()): ;?>
			<?php if($slider_query->have_posts()): ;?>
				<div class="owl-carousel owl-theme">
					<?php  while($slider_query->have_posts()) :  $slider_query->the_post(); 
					 $thumb = wp_get_attachment_image_src(get_post_thumbnail_id($post_id),'thumb-420x320', true);
					$display = get_field("slider_display");
					?>
					<?php if($display == true): ;?>
					<div class="item">
						<div class="owl_mask"></div>
						<img src="<?php echo  $thumb[0]; ?>" alt="">
						<h2 class="owl_title"><?php echo the_title();?></h2>
						<h3 class="read_more_owl"><a href="<?php echo the_permalink($post_id) ;?>">Czytaj więcej</a></h3>
					</div>
					<?php endif; endwhile;?>
				</div>
			<?php endif;?>
		<?php endif ;?>
		
	<?php do_action( 'sober_after_header' ); ?>

	<div id="content" class="site-content">
		<div class="container">
		
