<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Sober
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<section class="error-404 not-found container">

				<div class="page-content">
					<?php esc_html_e( '404 Strona, której szukasz nieistnieje', 'sober' ); ?>
					<h3><a href="<?php echo home_url() ;?>">Powrót do strony głównej</a></h3>
				</div><!-- .page-content -->

			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
