<?php
/**
 * Sober theme customizer
 *
 * @package Sober
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Sober_Customize {
	/**
	 * Customize settings
	 *
	 * @var array
	 */
	protected $config = array();

	/**
	 * The class constructor
	 *
	 * @param array $config
	 */
	public function __construct( $config ) {
		$this->config = $config;

		if ( ! class_exists( 'Kirki' ) ) {
			return;
		}

		$this->register();
	}

	/**
	 * Register settings
	 */
	public function register() {
		/**
		 * Add the theme configuration
		 */
		if ( ! empty( $this->config['theme'] ) ) {
			Kirki::add_config( $this->config['theme'], array(
				'capability'  => 'edit_theme_options',
				'option_type' => 'theme_mod',
			) );
		}

		/**
		 * Add panels
		 */
		if ( ! empty( $this->config['panels'] ) ) {
			foreach ( $this->config['panels'] as $panel => $settings ) {
				Kirki::add_panel( $panel, $settings );
			}
		}

		/**
		 * Add sections
		 */
		if ( ! empty( $this->config['sections'] ) ) {
			foreach ( $this->config['sections'] as $section => $settings ) {
				Kirki::add_section( $section, $settings );
			}
		}

		/**
		 * Add fields
		 */
		if ( ! empty( $this->config['theme'] ) && ! empty( $this->config['fields'] ) ) {
			foreach ( $this->config['fields'] as $name => $settings ) {
				if ( ! isset( $settings['settings'] ) ) {
					$settings['settings'] = $name;
				}

				Kirki::add_field( $this->config['theme'], $settings );
			}
		}
	}

	/**
	 * Get config ID
	 *
	 * @return string
	 */
	public function get_theme() {
		return $this->config['theme'];
	}

	/**
	 * Get customize setting value
	 *
	 * @param string $name
	 *
	 * @return bool|string
	 */
	public function get_option( $name ) {
		if ( ! isset( $this->config['fields'][ $name ] ) ) {
			return false;
		}

		$default = isset( $this->config['fields'][ $name ]['default'] ) ? $this->config['fields'][ $name ]['default'] : false;

		return get_theme_mod( $name, $default );
	}
}

/**
 * This is a short hand function for getting setting value from customizer
 *
 * @param string $name
 *
 * @return bool|string
 */
function sober_get_option( $name ) {
	global $sober_customize;

	if ( empty( $sober_customize ) ) {
		return false;
	}

	if ( class_exists( 'Kirki' ) ) {
		$value = Kirki::get_option( $sober_customize->get_theme(), $name );
	} else {
		$value = $sober_customize->get_option( $name );
	}

	return apply_filters( 'sober_get_option', $value, $name );
}

/**
 * Move some default sections to `general` panel that registered by theme
 *
 * @param object $wp_customize
 */
function sober_customize_modify( $wp_customize ) {
	$wp_customize->get_section( 'title_tagline' )->panel     = 'general';
	$wp_customize->get_section( 'static_front_page' )->panel = 'general';
}

add_action( 'customize_register', 'sober_customize_modify' );

function sober_customize_settings() {
	$settings = array(
		'theme' => 'sober',
	);

	// Register panels
	$panels = array(
		'general' => array(
			'priority' => 10,
			'title'    => esc_html__( 'General', 'sober' ),
		),
		'header'  => array(
			'priority' => 210,
			'title'    => esc_html__( 'Header', 'sober' ),
		),
		'shop'    => array(
			'priority' => 250,
			'title'    => esc_html__( 'Shop', 'sober' ),
		),
		'mobile'  => array(
			'priority' => 450,
			'title'    => esc_html__( 'Mobile', 'sober' ),
		),
	);

	// Register sections
	$sections = array(
		'background'    => array(
			'title'    => esc_html__( 'Background', 'sober' ),
			'priority' => 15,
		),
		'layout'        => array(
			'title'    => esc_html__( 'Layout', 'sober' ),
			'priority' => 20,
		),
		'topbar'        => array(
			'title'    => esc_html__( 'Topbar', 'sober' ),
			'priority' => 10,
			'panel'    => 'header',
		),
		'header'        => array(
			'title'    => esc_html__( 'Header', 'sober' ),
			'priority' => 20,
			'panel'    => 'header',
		),
		'logo'          => array(
			'title'    => esc_html__( 'Logo', 'sober' ),
			'priority' => 30,
			'panel'    => 'header',
		),
		'page_header'   => array(
			'title'    => esc_html__( 'Page Header', 'sober' ),
			'priority' => 230,
		),
		'blog'          => array(
			'title'    => esc_html__( 'Blog', 'sober' ),
			'priority' => 250,
		),
		'footer'        => array(
			'title'    => esc_html__( 'Footer', 'sober' ),
			'priority' => 400,
		),
		'mobile_header' => array(
			'title'    => esc_html__( 'Header', 'sober' ),
			'panel'    => 'mobile',
			'priority' => 10,
		),
		'mobile_menu'   => array(
			'title'    => esc_html__( 'Menu', 'sober' ),
			'panel'    => 'mobile',
			'priority' => 20,
		),
	);

	// Register fields
	$fields = array(
		// Background
		'404_bg'                 => array(
			'type'        => 'image',
			'label'       => esc_html__( '404 Page', 'sober' ),
			'description' => esc_html__( 'Background image for not found page', 'sober' ),
			'section'     => 'background',
			'default'     => '',
		),
		// Layout
		'layout_default'         => array(
			'type'        => 'radio-image',
			'label'       => esc_html__( 'Default Layout', 'sober' ),
			'description' => esc_html__( 'Default layout of blog and other pages', 'sober' ),
			'section'     => 'layout',
			'default'     => 'single-right',
			'choices'     => array(
				'no-sidebar'   => get_template_directory_uri() . '/images/options/sidebars/empty.png',
				'single-left'  => get_template_directory_uri() . '/images/options/sidebars/single-left.png',
				'single-right' => get_template_directory_uri() . '/images/options/sidebars/single-right.png',
			),
		),
		'layout_post'            => array(
			'type'        => 'radio-image',
			'label'       => esc_html__( 'Post Layout', 'sober' ),
			'description' => esc_html__( 'Default layout of single post', 'sober' ),
			'section'     => 'layout',
			'default'     => 'no-sidebar',
			'choices'     => array(
				'no-sidebar'   => get_template_directory_uri() . '/images/options/sidebars/empty.png',
				'single-left'  => get_template_directory_uri() . '/images/options/sidebars/single-left.png',
				'single-right' => get_template_directory_uri() . '/images/options/sidebars/single-right.png',
			),
		),
		'layout_page'            => array(
			'type'        => 'radio-image',
			'label'       => esc_html__( 'Page Layout', 'sober' ),
			'description' => esc_html__( 'Default layout of pages', 'sober' ),
			'section'     => 'layout',
			'default'     => 'no-sidebar',
			'choices'     => array(
				'no-sidebar'   => get_template_directory_uri() . '/images/options/sidebars/empty.png',
				'single-left'  => get_template_directory_uri() . '/images/options/sidebars/single-left.png',
				'single-right' => get_template_directory_uri() . '/images/options/sidebars/single-right.png',
			),
		),
		'layout_shop'            => array(
			'type'        => 'radio-image',
			'label'       => esc_html__( 'Shop Layout', 'sober' ),
			'description' => esc_html__( 'Default layout of shop pages', 'sober' ),
			'section'     => 'layout',
			'default'     => 'no-sidebar',
			'choices'     => array(
				'no-sidebar'   => get_template_directory_uri() . '/images/options/sidebars/empty.png',
				'single-left'  => get_template_directory_uri() . '/images/options/sidebars/single-left.png',
				'single-right' => get_template_directory_uri() . '/images/options/sidebars/single-right.png',
			),
		),
		// Topbar
		'topbar_enable'          => array(
			'type'    => 'toggle',
			'label'   => esc_html__( 'Show topbar', 'sober' ),
			'section' => 'topbar',
			'default' => 0,
		),
		'topbar_color'           => array(
			'type'     => 'radio',
			'label'    => esc_html__( 'Topbar Color', 'sober' ),
			'section'  => 'topbar',
			'default'  => 'dark',
			'priority' => 10,
			'choices'  => array(
				'dark'  => esc_html__( 'Dark', 'sober' ),
				'light' => esc_html__( 'Light', 'sober' ),
			),
		),
		'topbar_layout'          => array(
			'type'    => 'radio',
			'label'   => esc_html__( 'Topbar Layout', 'sober' ),
			'section' => 'topbar',
			'default' => '2-columns',
			'choices' => array(
				'2-columns' => esc_html__( '2 Columns', 'sober' ),
				'1-column'  => esc_html__( '1 Column', 'sober' ),
			),
		),
		'topbar_left'            => array(
			'type'            => 'radio',
			'label'           => esc_html__( 'Left Content', 'sober' ),
			'section'         => 'topbar',
			'default'         => 'switchers',
			'choices'         => array(
				'switchers'      => array(
					esc_html__( 'Currency and Language switchers', 'sober' ),
					esc_html__( 'It requires additional plugins installed', 'sober' ),
				),
				'custom_content' => array(
					esc_html__( 'Custom Content', 'sober' ),
					esc_html__( 'Custom content in center', 'sober' ),
				),
			),
			'active_callback' => array(
				array(
					'setting'  => 'topbar_layout',
					'operator' => '==',
					'value'    => '2-columns',
				),
			),
		),
		'topbar_content'         => array(
			'type'        => 'textarea',
			'label'       => esc_html__( 'Custom Content', 'sober' ),
			'description' => esc_html__( 'Allow HTML and Shortcodes', 'sober' ),
			'section'     => 'topbar',
			'default'     => '',
		),
		'topbar_closeable'       => array(
			'type'            => 'toggle',
			'label'           => esc_html__( 'Show Close Icon', 'sober' ),
			'section'         => 'topbar',
			'default'         => 0,
			'active_callback' => array(
				array(
					'setting'  => 'topbar_layout',
					'operator' => '==',
					'value'    => '1-column',
				),
			),
		),
		// Header layout
		'header_layout'          => array(
			'type'    => 'select',
			'label'   => esc_html__( 'Header Layout', 'sober' ),
			'section' => 'header',
			'default' => 'v1',
			'choices' => array(
				'v1' => esc_html__( 'Header v1', 'sober' ),
				'v2' => esc_html__( 'Header v2', 'sober' ),
				'v3' => esc_html__( 'Header v3', 'sober' ),
				'v4' => esc_html__( 'Header v4', 'sober' ),
				'v5' => esc_html__( 'Header v5', 'sober' ),
				'v6' => esc_html__( 'Header v6', 'sober' ),
			),
		),
		'header_bg'              => array(
			'type'    => 'radio',
			'label'   => esc_html__( 'Header Background', 'sober' ),
			'section' => 'header',
			'default' => 'white',
			'choices' => array(
				'white'       => esc_html__( 'White', 'sober' ),
				'transparent' => esc_html__( 'Transparent', 'sober' ),
			),
		),
		'header_text_color'      => array(
			'type'            => 'radio',
			'label'           => esc_html__( 'Header Text Color', 'sober' ),
			'description'     => esc_html__( 'Text light only apply for transparent header', 'sober' ),
			'section'         => 'header',
			'default'         => 'dark',
			'choices'         => array(
				'light' => esc_html__( 'Light', 'sober' ),
				'dark'  => esc_html__( 'Dark', 'sober' ),
			),
			'active_callback' => array(
				array(
					'setting'  => 'header_bg',
					'operator' => '==',
					'value'    => 'transparent',
				),
			),
		),
		'header_hover'           => array(
			'type'            => 'toggle',
			'label'           => esc_html__( 'Header Hover', 'sober' ),
			'description'     => esc_html__( 'Enable hover effect for transparent header', 'sober' ),
			'section'         => 'header',
			'default'         => true,
			'active_callback' => array(
				array(
					'setting'  => 'header_bg',
					'operator' => '==',
					'value'    => 'transparent',
				),
			),
		),
		'header_sticky'          => array(
			'type'        => 'select',
			'label'       => esc_html__( 'Sticky Header', 'sober' ),
			'description' => esc_html__( 'Make header always visible on top of site', 'sober' ),
			'section'     => 'header',
			'default'     => 'none',
			'choices'     => array(
				'none'   => esc_html__( 'Disable', 'sober' ),
				'normal' => esc_html__( 'Normal Sticky Header', 'sober' ),
				'smart'  => esc_html__( 'Smart Sticky Header', 'sober' ),
			),
		),
		'menu_animation'          => array(
			'type'        => 'select',
			'label'       => esc_html__( 'Menu Hover Animation', 'sober' ),
			'section'     => 'header',
			'default'     => 'fade',
			'choices'     => array(
				'none'  => esc_html__( 'No Animation', 'sober' ),
				'fade'  => esc_html__( 'Fade', 'sober' ),
				'slide' => esc_html__( 'Slide', 'sober' ),
			),
		),
		// Logo
		'logo_type'              => array(
			'type'    => 'radio',
			'label'   => esc_html__( 'Logo Type', 'sober' ),
			'section' => 'logo',
			'default' => 'image',
			'choices' => array(
				'image' => esc_html__( 'Image', 'sober' ),
				'text'  => esc_html__( 'Text', 'sober' ),
			),
		),
		'logo_text'              => array(
			'type'            => 'text',
			'label'           => esc_html__( 'Logo Text', 'sober' ),
			'section'         => 'logo',
			'default'         => get_bloginfo( 'name' ),
			'active_callback' => array(
				array(
					'setting'  => 'logo_type',
					'operator' => '==',
					'value'    => 'text',
				),
			),
		),
		'logo_font'              => array(
			'type'            => 'typography',
			'label'           => esc_html__( 'Logo Font', 'sober' ),
			'section'         => 'logo',
			'default'         => array(
				'font-family'    => 'Poppins',
				'variant'        => '700',
				'font-size'      => '30px',
				'letter-spacing' => '0',
				'subsets'        => array( 'latin-ext' ),
				'text-transform' => 'uppercase',
			),
			'output'          => array(
				array(
					'element' => '.site-branding .logo',
				),
			),
			'active_callback' => array(
				array(
					'setting'  => 'logo_type',
					'operator' => '==',
					'value'    => 'text',
				),
			),
		),
		'logo'                   => array(
			'type'            => 'image',
			'label'           => esc_html__( 'Logo', 'sober' ),
			'section'         => 'logo',
			'default'         => '',
			'active_callback' => array(
				array(
					'setting'  => 'logo_type',
					'operator' => '==',
					'value'    => 'image',
				),
			),
		),
		'logo_light'             => array(
			'type'            => 'image',
			'label'           => esc_html__( 'Logo Light', 'sober' ),
			'section'         => 'logo',
			'default'         => '',
			'active_callback' => array(
				array(
					'setting'  => 'logo_type',
					'operator' => '==',
					'value'    => 'image',
				),
			),
		),
		'logo_width'             => array(
			'type'            => 'number',
			'label'           => esc_html__( 'Logo Width', 'sober' ),
			'section'         => 'logo',
			'default'         => '',
			'active_callback' => array(
				array(
					'setting'  => 'logo_type',
					'operator' => '==',
					'value'    => 'image',
				),
			),
		),
		'logo_height'            => array(
			'type'            => 'number',
			'label'           => esc_html__( 'Logo Height', 'sober' ),
			'section'         => 'logo',
			'default'         => '',
			'active_callback' => array(
				array(
					'setting'  => 'logo_type',
					'operator' => '==',
					'value'    => 'image',
				),
			),
		),
		'logo_position'          => array(
			'type'    => 'spacing',
			'label'   => esc_html__( 'Logo Margin', 'sober' ),
			'section' => 'logo',
			'default' => array(
				'top'    => '0',
				'bottom' => '0',
				'left'   => '0',
				'right'  => '0',
			),
		),
		// Page header
		'page_header_enable'     => array(
			'type'    => 'toggle',
			'label'   => esc_html__( 'Show Page Header', 'sober' ),
			'section' => 'page_header',
			'default' => 1,
		),
		'show_breadcrumb'        => array(
			'type'            => 'toggle',
			'label'           => esc_html__( 'Show Breadcrumb', 'sober' ),
			'section'         => 'page_header',
			'default'         => 1,
			'active_callback' => array(
				array(
					'setting'  => 'page_header_enable',
					'operator' => '==',
					'value'    => true,
				),
			),
		),
		'page_header_style'      => array(
			'type'            => 'select',
			'label'           => esc_html__( 'Page Header Style', 'sober' ),
			'section'         => 'page_header',
			'default'         => 'normal',
			'choices'         => array(
				'normal'  => esc_html__( 'Normal', 'sober' ),
				'minimal' => esc_html__( 'Minimal', 'sober' ),
			),
			'active_callback' => array(
				array(
					'setting'  => 'page_header_enable',
					'operator' => '==',
					'value'    => true,
				),
			),
		),
		'page_header_parallax'   => array(
			'type'            => 'select',
			'label'           => esc_html__( 'Page Header Parallax', 'sober' ),
			'description'     => esc_html__( 'Select header parallax animation', 'sober' ),
			'section'         => 'page_header',
			'default'         => 'none',
			'choices'         => array(
				'none' => esc_html__( 'No Parallax', 'sober' ),
				'up'   => esc_html__( 'Move Up', 'sober' ),
				'down' => esc_html__( 'Move Down', 'sober' ),
			),
			'active_callback' => array(
				array(
					'setting'  => 'page_header_enable',
					'operator' => '==',
					'value'    => true,
				),
				array(
					'setting'  => 'page_header_style',
					'operator' => '==',
					'value'    => 'normal',
				),
			),
		),
		'page_header_bg'         => array(
			'type'            => 'image',
			'label'           => esc_html__( 'Page Header Image', 'sober' ),
			'description'     => esc_html__( 'The default background image for page header', 'sober' ),
			'section'         => 'page_header',
			'default'         => '',
			'active_callback' => array(
				array(
					'setting'  => 'page_header_enable',
					'operator' => '==',
					'value'    => true,
				),
				array(
					'setting'  => 'page_header_style',
					'operator' => '==',
					'value'    => 'normal',
				),
			),
		),
		'page_header_text_color' => array(
			'type'            => 'select',
			'label'           => esc_html__( 'Text Color', 'sober' ),
			'section'         => 'page_header',
			'default'         => 'dark',
			'choices'         => array(
				'dark'  => esc_html__( 'Dark', 'sober' ),
				'light' => esc_html__( 'Light', 'sober' ),
			),
			'active_callback' => array(
				array(
					'setting'  => 'page_header_enable',
					'operator' => '==',
					'value'    => true,
				),
				array(
					'setting'  => 'page_header_style',
					'operator' => '!=',
					'value'    => 'minimal',
				),
			),
		),
		// Blog
		'post_header'            => array(
			'type'        => 'sortable',
			'label'       => esc_html__( 'Post Header', 'sober' ),
			'description' => esc_html__( 'Drag and drop these fields to re-order the layout of single post header.', 'sober' ),
			'section'     => 'blog',
			'default'     => array( 'meta', 'title', 'share', 'image' ),
			'choices'     => array(
				'meta'  => esc_html__( 'Post Meta', 'sober' ),
				'title' => esc_html__( 'Post Title', 'sober' ),
				'share' => esc_html__( 'Sharing Icons', 'sober' ),
				'image' => esc_html__( 'Post Image', 'sober' ),
			),
		),
		'post_navigation'        => array(
			'type'    => 'toggle',
			'label'   => esc_html__( 'Post Navigation', 'sober' ),
			'section' => 'blog',
			'default' => true,
		),
		'post_author_box'        => array(
			'type'    => 'toggle',
			'label'   => esc_html__( 'Author Box', 'sober' ),
			'section' => 'blog',
			'default' => true,
		),
		'post_related_posts'     => array(
			'type'    => 'toggle',
			'label'   => esc_html__( 'Related Posts', 'sober' ),
			'section' => 'blog',
			'default' => true,
		),
		'blog_layout'            => array(
			'type'    => 'radio',
			'label'   => esc_html__( 'Blog Layout', 'sober' ),
			'section' => 'blog',
			'default' => 'classic',
			'choices' => array(
				'classic' => esc_html__( 'Classic', 'sober' ),
				'grid'    => esc_html__( 'Grid', 'sober' ),
			),
		),
		'blog_categories'        => array(
			'type'    => 'toggle',
			'label'   => esc_html__( 'Show Category List', 'sober' ),
			'description' => esc_html__( 'Display category list on top of blog posts', 'sober' ),
			'section' => 'blog',
			'default' => true,
		),
		'excerpt_length'         => array(
			'type'    => 'number',
			'label'   => esc_html__( 'Excerpt Length', 'sober' ),
			'section' => 'blog',
			'default' => 30,
		),

		// Footer
		'footer_content_enable'  => array(
			'type'        => 'toggle',
			'label'       => esc_html__( 'Enable Footer Content', 'sober' ),
			'description' => esc_html__( 'Display extra content above the footer', 'sober' ),
			'section'     => 'footer',
			'default'     => true,
		),
		'footer_gotop'           => array(
			'type'            => 'toggle',
			'label'           => esc_html__( 'Enable "Go To Top" Icon', 'sober' ),
			'section'         => 'footer',
			'default'         => true,
			'active_callback' => array(
				array(
					'setting'  => 'footer_content_enable',
					'operator' => '==',
					'value'    => true,
				),
			),
		),
		'footer_content'         => array(
			'type'            => 'textarea',
			'label'           => esc_html__( 'Footer Extra Content', 'sober' ),
			'section'         => 'footer',
			'default'         => '',
			'active_callback' => array(
				array(
					'setting'  => 'footer_content_enable',
					'operator' => '==',
					'value'    => true,
				),
			),
		),
		'footer_copyright'       => array(
			'type'        => 'textarea',
			'label'       => esc_html__( 'Footer Copyright', 'sober' ),
			'description' => esc_html__( 'Display copyright info on the left side of footer', 'sober' ),
			'section'     => 'footer',
			'default'     => sprintf( esc_html__( 'Copyright %s %s', 'sober' ), "&copy;", date( 'Y' ) ),
		),
		'footer_social_extra'    => array(
			'type'        => 'textarea',
			'label'       => esc_html__( 'Footer Right Content', 'sober' ),
			'description' => esc_html__( 'Add extra content on the right side of footer', 'sober' ),
			'section'     => 'footer',
			'default'     => '',
		),
		// Mobile Header
		'mobile_cart_badge'      => array(
			'type'        => 'toggle',
			'label'       => esc_html__( 'Cart Counter Badge', 'sober' ),
			'description' => esc_html__( 'Adds a badge beside cart icon to show number of items', 'sober' ),
			'section'     => 'mobile_header',
			'default'     => false,
		),
		// Mobile Menu
		'mobile_menu_close'      => array(
			'type'        => 'toggle',
			'label'       => esc_html__( 'Close Icon', 'sober' ),
			'description' => esc_html__( 'Adds a close icon on top of mobile menu', 'sober' ),
			'section'     => 'mobile_menu',
			'default'     => false,
		),
		'mobile_menu_width'      => array(
			'type'        => 'slider',
			'label'       => esc_html__( 'Mobile Menu Width', 'sober' ),
			'description' => esc_html__( 'Change mobile menu width', 'sober' ),
			'section'     => 'mobile_menu',
			'transport'   => 'auto',
			'default'     => 85,
			'choices'     => array(
				'min'  => '50',
				'max'  => '90',
				'step' => '1',
			),
			'output'      => array(
				array(
					'element'     => '.mobile-menu',
					'property'    => 'width',
					'units'       => '%',
					'media_query' => '@media screen and (max-width: 767px)',
				),
			),
		),
		'mobile_menu_search'     => array(
			'type'        => 'toggle',
			'label'       => esc_html__( 'Mobile Menu Search', 'sober' ),
			'description' => esc_html__( 'Show search form in the mobile menu', 'sober' ),
			'section'     => 'mobile_menu',
			'default'     => true,
		),
		'mobile_menu_top'        => array(
			'type'        => 'multicheck',
			'label'       => esc_html__( 'Mobile Menu Top', 'sober' ),
			'description' => esc_html__( 'Show extra items on top of the mobile menu', 'sober' ),
			'section'     => 'mobile_menu',
			'choices'     => array(
				'currency' => esc_html__( 'Currency Switcher (require plugin)', 'sober' ),
				'language' => esc_html__( 'Language Switcher (require plugin)', 'sober' ),
			),
		),
		'mobile_menu_bottom'     => array(
			'type'        => 'multicheck',
			'label'       => esc_html__( 'Mobile Menu Bottom', 'sober' ),
			'description' => esc_html__( 'Append items at end of the mobile menu', 'sober' ),
			'section'     => 'mobile_menu',
			'default'     => array( 'cart', 'login' ),
			'choices'     => array(
				'cart'  => esc_html__( 'Shopping Cart', 'sober' ),
				'login' => esc_html__( 'Login/Account', 'sober' ),
			),
		),
	);

	// Setting fields for WooCommerce
	if ( function_exists( 'WC' ) ) {
		$panels['shop'] = array(
			'priority' => 250,
			'title'    => esc_html__( 'Shop', 'sober' ),
		);

		$sections['catalog'] = array(
			'title'    => esc_html__( 'Catalog', 'sober' ),
			'priority' => 10,
			'panel'    => 'shop',
		);

		$sections['product'] = array(
			'title'    => esc_html__( 'Product', 'sober' ),
			'priority' => 20,
			'panel'    => 'shop',
		);

		$sections['mobile_shop'] = array(
			'title'    => esc_html__( 'Shop Catalog', 'sober' ),
			'priority' => 30,
			'panel'    => 'mobile',
		);

		$fields = array_merge( $fields, array(
			'shop_page_header_bg'             => array(
				'type'            => 'image',
				'label'           => esc_html__( 'Shop Page Header Image', 'sober' ),
				'description'     => esc_html__( 'The default background image for page header on shop pages', 'sober' ),
				'section'         => 'page_header',
				'default'         => '',
				'active_callback' => array(
					array(
						'setting'  => 'page_header_enable',
						'operator' => '==',
						'value'    => true,
					),
					array(
						'setting'  => 'page_header_style',
						'operator' => '==',
						'value'    => 'normal',
					),
				),
			),
			'shop_page_header_text_color'     => array(
				'type'            => 'select',
				'label'           => esc_html__( 'Shop Page Header Text Color', 'sober' ),
				'section'         => 'page_header',
				'default'         => 'dark',
				'choices'         => array(
					'dark'  => esc_html__( 'Dark', 'sober' ),
					'light' => esc_html__( 'Light', 'sober' ),
				),
				'active_callback' => array(
					array(
						'setting'  => 'page_header_enable',
						'operator' => '==',
						'value'    => true,
					),
					array(
						'setting'  => 'page_header_style',
						'operator' => '!=',
						'value'    => 'minimal',
					),
				),
			),
			// Shop catalog
			'products_toggle'                 => array(
				'type'        => 'toggle',
				'label'       => esc_html__( 'Product Tabs', 'sober' ),
				'description' => esc_html__( 'Enable product tabs on top left', 'sober' ),
				'section'     => 'catalog',
				'default'     => true,
			),
			'products_toggle_type'            => array(
				'type'            => 'select',
				'label'           => esc_html__( 'Products Tabs Type', 'sober' ),
				'description'     => esc_html__( 'Select how to group products in tabs', 'sober' ),
				'section'         => 'catalog',
				'default'         => 'group',
				'choices'         => array(
					'group'    => esc_html__( 'Groups', 'sober' ),
					'category' => esc_html__( 'Categories', 'sober' ),
					'tag'      => esc_html__( 'Tags', 'sober' ),
				),
				'active_callback' => array(
					array(
						'setting'  => 'products_toggle',
						'operator' => '==',
						'value'    => true,
					),
				),
			),
			'products_toggle_groups'          => array(
				'type'            => 'multicheck',
				'label'           => esc_html__( 'Products Tab Groups', 'sober' ),
				'description'     => esc_html__( 'Select how to group products in tabs', 'sober' ),
				'section'         => 'catalog',
				'default'         => array( 'featured', 'new', 'sale' ),
				'choices'         => array(
					'featured' => esc_html__( 'Hot Products', 'sober' ),
					'new'      => esc_html__( 'New Products', 'sober' ),
					'sale'     => esc_html__( 'Sale Products', 'sober' ),
				),
				'active_callback' => array(
					array(
						'setting'  => 'products_toggle',
						'operator' => '==',
						'value'    => true,
					),
					array(
						'setting'  => 'products_toggle_type',
						'operator' => '==',
						'value'    => 'group',
					),
				),
			),
			'products_toggle_category_amount' => array(
				'type'            => 'number',
				'label'           => esc_html__( 'Number Of Categories', 'sober' ),
				'description'     => esc_html__( 'Amount of top categories to get', 'sober' ),
				'section'         => 'catalog',
				'default'         => 3,
				'active_callback' => array(
					array(
						'setting'  => 'products_toggle',
						'operator' => '==',
						'value'    => true,
					),
					array(
						'setting'  => 'products_toggle_type',
						'operator' => '==',
						'value'    => 'category',
					),
				),
			),
			'products_toggle_tags'            => array(
				'type'            => 'text',
				'label'           => esc_html__( 'Tags', 'sober' ),
				'description'     => esc_html__( 'Enter tags for filter tabs. Separate by commas', 'sober' ),
				'section'         => 'catalog',
				'default'         => '',
				'active_callback' => array(
					array(
						'setting'  => 'products_toggle',
						'operator' => '==',
						'value'    => true,
					),
					array(
						'setting'  => 'products_toggle_type',
						'operator' => '==',
						'value'    => 'tag',
					),
				),
			),
			'products_filter'                 => array(
				'type'        => 'toggle',
				'label'       => esc_html__( 'Products Filter', 'sober' ),
				'description' => esc_html__( 'Show filter icon on the right side', 'sober' ),
				'tooltip'     => esc_html__( 'This requires Shop Filter sidebar must has at least one widget.', 'sober' ),
				'section'     => 'catalog',
				'default'     => true,
			),
			'product_onsale_off'              => array(
				'type'        => 'toggle',
				'label'       => esc_html__( 'Onsale ribbon with "OFF"', 'sober' ),
				'description' => esc_html__( 'Adds "OFF" word after the percentage for onsale products', 'sober' ),
				'section'     => 'catalog',
				'default'     => false,
			),
			'product_hover_thumbnail'         => array(
				'type'        => 'toggle',
				'label'       => esc_html__( 'Show Hover Thumbnail', 'sober' ),
				'description' => esc_html__( 'Show different product thumbnail when hover', 'sober' ),
				'section'     => 'catalog',
				'default'     => true,
			),
			'product_hide_outstock_price'     => array(
				'type'        => 'toggle',
				'label'       => esc_html__( 'Hide Out Of Stock Price', 'sober' ),
				'description' => esc_html__( 'Hide the price if a product is out of stock', 'sober' ),
				'section'     => 'catalog',
				'default'     => false,
			),
			'product_quickview'               => array(
				'type'        => 'toggle',
				'label'       => esc_html__( 'Product Quick View', 'sober' ),
				'description' => esc_html__( 'Show the product modal when a product clicked', 'sober' ),
				'section'     => 'catalog',
				'default'     => true,
			),
			'product_quickview_behavior'      => array(
				'type'            => 'select',
				'label'           => esc_html__( 'Show Quick View When', 'sober' ),
				'section'         => 'catalog',
				'default'         => 'image',
				'choices'         => array(
					'image'       => esc_html__( 'Click on image', 'sober' ),
					'view_button' => esc_html__( 'Click on quick-view button', 'sober' ),
					'buy_button'  => esc_html__( 'Click on buy button', 'sober' ),
					'title'       => esc_html__( 'Click on product title', 'sober' ),
				),
				'active_callback' => array(
					array(
						'setting'  => 'product_quickview',
						'operator' => '==',
						'value'    => true,
					),
				),
			),
			'product_columns'                 => array(
				'type'    => 'select',
				'label'   => esc_html__( 'Product Columns', 'sober' ),
				'section' => 'catalog',
				'default' => '5',
				'choices' => array(
					'4' => esc_html__( '4 Columns', 'sober' ),
					'5' => esc_html__( '5 Columns', 'sober' ),
					'6' => esc_html__( '6 Columns', 'sober' ),
				),
			),
			'products_per_page'               => array(
				'type'    => 'number',
				'label'   => esc_html__( 'Products Per Page', 'sober' ),
				'section' => 'catalog',
				'default' => 15,
			),
			'shop_nav_type'                   => array(
				'type'    => 'radio',
				'label'   => esc_html__( 'Navigation Type', 'sober' ),
				'section' => 'catalog',
				'default' => 'links',
				'choices' => array(
					'links'    => esc_html__( 'Numeric', 'sober' ),
					'ajax'     => esc_html__( 'Load more button', 'sober' ),
					'infinity' => esc_html__( 'Infinity Scroll', 'sober' ),
				),
			),
			// Product
			'product_newness'                 => array(
				'type'        => 'number',
				'label'       => esc_html__( 'Product Newness', 'sober' ),
				'description' => esc_html__( 'Display the "New" badge for how many days?', 'sober' ),
				'section'     => 'product',
				'default'     => 3,
			),
			'single_product_style'            => array(
				'type'    => 'select',
				'label'   => esc_html__( 'Single Product Style', 'sober' ),
				'section' => 'product',
				'default' => 'style-1',
				'choices' => array(
					'style-1' => esc_html__( 'Style 1', 'sober' ),
					'style-2' => esc_html__( 'Style 2', 'sober' ),
					'style-3' => esc_html__( 'Style 3', 'sober' ),
				),
			),
			'product_share'                   => array(
				'type'        => 'multicheck',
				'label'       => esc_html__( 'Product Share', 'sober' ),
				'description' => esc_html__( 'Select social media for sharing products', 'sober' ),
				'section'     => 'product',
				'default'     => array( 'facebook', 'twitter', 'pinterest' ),
				'choices'     => array(
					'facebook'  => esc_html__( 'Facebook', 'sober' ),
					'twitter'   => esc_html__( 'Twitter', 'sober' ),
					'pinterest' => esc_html__( 'Pinterest', 'sober' ),
				),
			),
			'product_extra_content'           => array(
				'type'        => 'textarea',
				'label'       => esc_html__( 'Extra Content', 'sober' ),
				'description' => esc_html__( 'Add extra content at the bottom of every product short description. Shortcodes and HTML are allowed.', 'sober' ),
				'section'     => 'product',
				'default'     => '',
			),
			// Mobile
			'mobile_shop_add_to_cart'         => array(
				'type'        => 'toggle',
				'label'       => esc_html__( 'Show Buttons', 'sober' ),
				'description' => esc_html__( 'Show add to cart & add to wishlist buttons', 'sober' ),
				'section'     => 'mobile_shop',
				'default'     => false,
			),
		) );
	}

	// Setting fields for Portfolio
	if ( get_option( 'sober_portfolio' ) ) {
		$panels['portfolio'] = array(
			'title'    => esc_html__( 'Portfolio', 'sober' ),
			'priority' => 350,
		);

		$sections['portfolio_archive'] = array(
			'title'    => esc_html__( 'Portfolio Page', 'sober' ),
			'priority' => 10,
			'panel'    => 'portfolio',
		);

		$sections['portfolio_single'] = array(
			'title'    => esc_html__( 'Project Page', 'sober' ),
			'priority' => 20,
			'panel'    => 'portfolio',
		);

		$fields = array_merge( $fields, array(
			'portfolio_page_header_bg'         => array(
				'type'            => 'image',
				'label'           => esc_html__( 'Portfolio Page Header Image', 'sober' ),
				'description'     => esc_html__( 'The background image for portfolio page header', 'sober' ),
				'section'         => 'page_header',
				'default'         => '',
				'active_callback' => array(
					array(
						'setting'  => 'page_header_enable',
						'operator' => '==',
						'value'    => true,
					),
					array(
						'setting'  => 'page_header_style',
						'operator' => '==',
						'value'    => 'normal',
					),
					array(
						'setting'  => 'portfolio_style',
						'operator' => '==',
						'value'    => 'masonry',
					),
				),
			),
			'portfolio_page_header_text_color' => array(
				'type'            => 'select',
				'label'           => esc_html__( 'Portfolio Page Header Text Color', 'sober' ),
				'section'         => 'page_header',
				'default'         => 'dark',
				'choices'         => array(
					'dark'  => esc_html__( 'Dark', 'sober' ),
					'light' => esc_html__( 'Light', 'sober' ),
				),
				'active_callback' => array(
					array(
						'setting'  => 'page_header_enable',
						'operator' => '==',
						'value'    => true,
					),
					array(
						'setting'  => 'page_header_style',
						'operator' => '!=',
						'value'    => 'minimal',
					),
					array(
						'setting'  => 'portfolio_style',
						'operator' => '==',
						'value'    => 'masonry',
					),
				),
			),
			// Portfolio archive
			'portfolio_filter'                 => array(
				'type'        => 'toggle',
				'label'       => esc_html__( 'Portfolio Filter', 'sober' ),
				'description' => esc_html__( 'Enable portfolio filter on the archive page', 'sober' ),
				'section'     => 'portfolio_archive',
				'default'     => true,
			),
			'portfolio_style'                  => array(
				'type'    => 'select',
				'label'   => esc_html__( 'Portfolio Style', 'sober' ),
				'section' => 'portfolio_archive',
				'default' => 'classic',
				'choices' => array(
					'classic'   => esc_html__( 'Classic', 'sober' ),
					'fullwidth' => esc_html__( 'Full Width', 'sober' ),
					'masonry'   => esc_html__( 'Masonry', 'sober' ),
				),
			),
			// Portfolio single
			'project_navigation'               => array(
				'type'        => 'toggle',
				'label'       => esc_html__( 'Single Navigation', 'sober' ),
				'description' => esc_html__( 'Enable next/previous navigation', 'sober' ),
				'section'     => 'portfolio_single',
				'default'     => true,
			),
			'project_nav_text_next'            => array(
				'type'            => 'text',
				'label'           => esc_html__( 'Next Link Text', 'sober' ),
				'section'         => 'portfolio_single',
				'default'         => esc_html__( 'Next Project', 'sober' ),
				'active_callback' => array(
					array(
						'setting'  => 'project_navigation',
						'operator' => '==',
						'value'    => true,
					),
				),
			),
			'project_nav_text_prev'            => array(
				'type'            => 'text',
				'label'           => esc_html__( 'Prev Link Text', 'sober' ),
				'section'         => 'portfolio_single',
				'default'         => esc_html__( 'Previous Project', 'sober' ),
				'active_callback' => array(
					array(
						'setting'  => 'project_navigation',
						'operator' => '==',
						'value'    => true,
					),
				),
			),
		) );
	}

	$settings['panels']   = apply_filters( 'sober_customize_panels', $panels );
	$settings['sections'] = apply_filters( 'sober_customize_sections', $sections );
	$settings['fields']   = apply_filters( 'sober_customize_fields', $fields );

	return $settings;
}

/**
 * Global variable
 */
$sober_customize = new Sober_Customize( sober_customize_settings() );
