<?php
/**
 * Customize WooCommerce templates
 *
 * @package Sober
 */

/**
 * Class for all WooCommerce template modification
 *
 * @version 1.0
 */
class Sober_WooCommerce {
	/**
	 * The single instance of the class
	 *
	 * @var Sober_WooCommerce
	 */
	protected static $instance = null;

	/**
	 * Number of days to keep set a product as a new one
	 * @var int
	 */
	protected $new_duration;

	/**
	 * Main instance
	 *
	 * @return Sober_WooCommerce
	 */
	public static function instance() {
		if ( null == self::$instance ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	/**
	 * Construction function
	 */
	public function __construct() {
		// Check if Woocomerce plugin is actived
		if ( ! in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
			return;
		}

		$this->new_duration = sober_get_option( 'product_newness' );

		$this->parse_query();
		$this->hooks();
	}

	/**
	 * Parse request to change the shop columns and products per page
	 */
	public function parse_query() {
		if ( isset( $_GET['shop_columns'] ) && in_array( intval( $_GET['shop_columns'] ), array( 4, 5, 6 ) ) ) {
			wc_setcookie( 'product_columns', intval( $_GET['shop_columns'] ), 6 * 60 * 24 * 30 );
			WC()->session->set( 'product_columns', intval( $_GET['shop_columns'] ) );
		}
	}

	/**
	 * Hooks to WooCommerce actions, filters
	 *
	 * @since  1.0
	 * @return void
	 */
	public function hooks() {
		// Need an early hook to ajaxify update mini shop cart
		add_filter( 'add_to_cart_fragments', array( $this, 'add_to_cart_fragments' ) );
		add_filter( 'add_to_wishlist_fragments', array( $this, 'add_to_wishlist_fragments' ) );

		// WooCommerce Styles
		add_filter( 'woocommerce_enqueue_styles', array( $this, 'wc_styles' ) );

		// Add Bootstrap classes
		add_filter( 'post_class', array( $this, 'product_class' ), 50, 3 );

		// Change shop columns
		add_filter( 'loop_shop_columns', array( $this, 'shop_columns' ), 20 );
		add_filter( 'loop_shop_per_page', array( $this, 'products_per_page' ), 20 );

		// Add badges
		remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash' );
		remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash' );
		add_action( 'woocommerce_before_shop_loop_item_title', array( $this, 'product_ribbons' ) );
		add_action( 'woocommerce_before_single_product_summary', array( $this, 'product_ribbons' ), 7 );

		// Wrap product thumbnail
		add_action( 'woocommerce_before_shop_loop_item', array( $this, 'open_loop_thumbnail_wrapper' ), 5 );
		add_action( 'woocommerce_before_shop_loop_item_title', array( $this, 'close_loop_thumbnail_wrapper' ), 30 );

		// Change product link position
		remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );
		add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_link_close', 20 );
		remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart' );
		add_action( 'woocommerce_before_shop_loop_item_title', array( $this, 'show_product_loop_buttons' ), 25 );

		// Adds hovered thumbnail to loop product
		add_action( 'woocommerce_before_shop_loop_item_title', array( $this, 'show_product_loop_hover_thumbnail' ) );

		// Add link to product title in shop loop
		remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title' );
		add_action( 'woocommerce_shop_loop_item_title', array( $this, 'show_product_loop_title' ) );

		// Remove stars in shop loop
		remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );

		// Change next and prev icon
		add_filter( 'woocommerce_pagination_args', array( $this, 'pagination_args' ) );

		// Add toolbars for shop page
		add_filter( 'woocommerce_show_page_title', '__return_false' );
		remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
		remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
		add_action( 'woocommerce_before_shop_loop', array( $this, 'shop_toolbar' ) );

		// Remove breadcrumb, use theme's instead
		add_filter( 'woocommerce_breadcrumb_defaults', array( $this, 'breadcrumb_args' ) );
		remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );

		if ( sober_get_option( 'product_hide_outstock_price' ) ) {
			add_filter( 'woocommerce_get_price_html', array( $this, 'price_html' ), 10, 2 );
		}

		/**
		 * Single products hooks
		 */
		if ( is_singular( 'product' ) ) {
			add_action( 'woocommerce_before_main_content', array( $this, 'product_breadcrumb' ), 20 );
		}

		// Adds breadcrumb and product navigation on top of product
		add_action( 'woocommerce_before_single_product_summary', array( $this, 'product_toolbar' ), 5 );

		// Wrap images and summary into a div
		add_action( 'woocommerce_before_single_product_summary', array( $this, 'open_product_summary' ), 5 );
		add_action( 'woocommerce_after_single_product_summary', array( $this, 'close_product_summary' ), 5 );

		// Change thumbnail size
		add_filter( 'single_product_small_thumbnail_size', array( $this, 'small_thumbnail_size' ) );

		// Prepare images for quickview modal
		if ( sober_get_option( 'product_quickview' ) || 'style-1' == sober_get_option( 'single_product_style' ) ) {
			add_action( 'woocommerce_product_thumbnails', array( $this, 'prepare_quickview_images' ), 100 );
		}

		// Wrap product summary for sticky description on product style 1
		if ( 'style-1' == sober_get_option( 'single_product_style' ) ) {
			add_action( 'woocommerce_single_product_summary', array( $this, 'open_sticky_description' ), 0 );
			add_action( 'woocommerce_single_product_summary', array( $this, 'close_sticky_description' ), 1000 );
		}

		// Reorder stars
		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
		add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 15 );

		// Reorder description
		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
		add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 10 );

		// Reorder price
		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
		add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 20 );

		// Add to wishlist button
		remove_action( 'woocommerce_after_add_to_cart_button', array(
			'Soo_Wishlist_Frontend',
			'single_product_button',
		) );
		add_action( 'woocommerce_after_add_to_cart_button', array( $this, 'single_product_wishlist_button' ) );
		add_filter( 'woocommerce_reset_variations_link', array( $this, 'reset_variations_link' ) );

		// Product share
		add_action( 'woocommerce_share', array( $this, 'share' ) );

		// Product extra content
		add_action( 'woocommerce_single_product_summary', array( $this, 'product_extra_content' ), 200 );

		// Change product tabs title
		add_filter( 'woocommerce_product_tabs', array( $this, 'product_tabs' ), 50 );

		// Remove tab heading
		add_filter( 'woocommerce_product_additional_information_heading', '__return_false' );
		add_filter( 'woocommerce_product_description_heading', '__return_false' );

		// Related and upsells columns
		add_filter( 'woocommerce_related_products_columns', array( $this, 'related_products_columns' ), 20 );
		add_filter( 'woocommerce_up_sells_columns', array( $this, 'related_products_columns' ), 20 );

		if ( 'style-3' == sober_get_option( 'single_product_style' ) ) {
			remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
			add_action( 'woocommerce_after_single_product', 'woocommerce_upsell_display' );
		}

		/**
		 * Cart page
		 */
		remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display' );
		add_action( 'woocommerce_after_cart', 'woocommerce_cross_sell_display' );
		add_filter( 'woocommerce_cross_sells_columns', array( $this, 'cross_sell_columns' ) );

		// Add billing title
		add_action( 'woocommerce_checkout_before_customer_details', array( $this, 'billing_title' ) );
	}

	/**
	 * Remove default woocommerce styles
	 *
	 * @since  1.0
	 *
	 * @param  array $styles
	 *
	 * @return array
	 */
	public function wc_styles( $styles ) {
		// unset( $styles['woocommerce-general'] );
		unset( $styles['woocommerce-layout'] );
		unset( $styles['woocommerce-smallscreen'] );

		return $styles;
	}

	/**
	 * Ajaxify update cart viewer
	 *
	 * @since 1.0
	 *
	 * @param array $fragments
	 *
	 * @return array
	 */
	public function add_to_cart_fragments( $fragments ) {
		$fragments['span.cart-counter'] = '<span class="count cart-counter">' . WC()->cart->get_cart_contents_count() . '</span>';

		return $fragments;
	}

	/**
	 * Ajaxify update wishlist viewer
	 *
	 * @param $fragments
	 *
	 * @return mixed
	 */
	public function add_to_wishlist_fragments( $fragments ) {
		if ( ! function_exists( 'Soo_Wishlist' ) ) {
			return $fragments;
		}

		$fragments['span.wishlist-counter'] = '<span class="count wishlist-counter">' . soow_count_products() . '</span>';

		$fragments['.sober-modal div.soo-wishlist'] = do_shortcode( '[soo_wishlist]' );

		return $fragments;
	}

	/**
	 * Change the shop columns
	 *
	 * @since  1.0.0
	 *
	 * @param  int $columns The default columns
	 *
	 * @return int
	 */
	public function shop_columns( $columns ) {
		if ( is_woocommerce() ) {
			$columns = ! is_null( WC()->session->get( 'product_columns' ) ) ? intval( WC()->session->get( 'product_columns' ) ) : intval( sober_get_option( 'product_columns' ) );
		}

		return $columns;
	}

	/**
	 * Change number of products per page
	 *
	 * @param int $limit
	 *
	 * @return int
	 */
	public function products_per_page( $limit ) {
		if ( is_woocommerce() ) {
			if ( ! is_null( WC()->session->get( 'product_columns' ) ) ) {
				$limit = 3 * intval( WC()->session->get( 'product_columns' ) );
			} else {
				$limit = intval( sober_get_option( 'products_per_page' ) );
			}
		}

		return $limit;
	}

	/**
	 * Change next and previous icon of pagination nav
	 *
	 * @since  1.0
	 */
	public function pagination_args( $args ) {
		$args['prev_text'] = '<svg viewBox="0 0 20 20"><use xlink:href="#left-arrow"></use></svg>';
		$args['next_text'] = '<svg viewBox="0 0 20 20"><use xlink:href="#right-arrow"></use></svg>';

		if ( sober_get_option( 'shop_nav_type' ) != 'links' ) {
			$loading           = '<span class="loading-icon"><span class="bubble"><span class="dot"></span></span><span class="bubble"><span class="dot"></span></span><span class="bubble"><span class="dot"></span></span></span>';
			$args['prev_text'] = '';
			$args['next_text'] = '<span class="button-text">' . esc_html__( 'Load More', 'sober' ) . '</span>' . $loading;
		}

		return $args;
	}

	/**
	 * Display a tool bar on top of product archive
	 *
	 * @since 1.0
	 */
	public function shop_toolbar() {
		$columns = ! is_null( WC()->session->get( 'product_columns' ) ) ? intval( WC()->session->get( 'product_columns' ) ) : intval( sober_get_option( 'product_columns' ) );
		$toggle  = sober_get_option( 'products_toggle' );

		if ( $toggle ) {
			$type = sober_get_option( 'products_toggle_type' );
			$tabs = array( '<li data-filter="*" class="line-hover active">' . esc_html__( 'All Products', 'sober' ) . '</li>' );

			if ( 'tag' == $type ) {
				$tags = trim( sober_get_option( 'products_toggle_tags' ) );
				$tags = array_filter( explode( ',', $tags ) );

				foreach ( $tags as $tag ) {
					$tag = trim( $tag );

					$tabs[] = sprintf(
						'<li data-filter=".product_tag-%s" class="line-hover">%s</li>',
						esc_attr( str_replace( array( ' ', '_' ), '-', strtolower( $tag ) ) ),
						esc_html( str_replace( '_', ' ', ucwords( $tag ) ) )
					);
				}
			} elseif ( 'category' == $type && ! is_product_category() ) {
				$categories = get_terms( array(
					'taxonomy' => 'product_cat',
					'orderby'  => 'count',
					'order'    => 'DESC',
					'number'   => intval( sober_get_option( 'products_toggle_category_amount' ) ),
				) );

				if ( $categories && ! is_wp_error( $categories ) ) {
					foreach ( $categories as $category ) {
						$tabs[] = sprintf( '<li data-filter=".product_cat-%s" class="line-hover">%s</li>', esc_attr( $category->slug ), esc_html( $category->name ) );
					}
				}
			} elseif ( 'group' == $type || is_product_category() ) {
				$groups = sober_get_option( 'products_toggle_groups' );

				if ( in_array( 'featured', $groups ) ) {
					$tabs[] = '<li data-filter=".featured" class="line-hover">' . esc_html__( 'Hot Products', 'sober' ) . '</li>';
				}

				if ( in_array( 'new', $groups ) ) {
					$tabs[] = '<li data-filter=".new" class="line-hover">' . esc_html__( 'New Products', 'sober' ) . '</li>';
				}

				if ( in_array( 'sale', $groups ) ) {
					$tabs[] = '<li data-filter=".sale" class="line-hover">' . esc_html__( 'Sales Products', 'sober' ) . '</li>';
				}
			}
		}
		?>

		<div class="shop-toolbar">
			<div class="row">
				<div class="col-sm-9 col-md-7 hidden-xs nav-filter">
					<?php if ( $toggle ) : ?>
						<ul class="products-filter clearfix">
							<?php echo implode( "\n", $tabs ) ?>
						</ul>
					<?php else : ?>
						<?php woocommerce_result_count(); ?>
					<?php endif; ?>
				</div>

				<div class="col-xs-12 col-sm-3 col-md-5 controls">
					<ul class="toolbar-control">
						<?php if ( $toggle ) : ?>
							<li class="data totals">
								<?php woocommerce_result_count() ?>
							</li>
						<?php endif; ?>
						<li class="data product-size">
							<a href="<?php echo esc_url( add_query_arg( array( 'shop_columns' => 6 ) ) ) ?>" class="small-size <?php echo 6 == $columns ? 'active' : '' ?>">
								<svg viewBox="0 0 15 15">
									<use xlink:href="#small-view-size"></use>
								</svg>
							</a>
							<a href="<?php echo esc_url( add_query_arg( array( 'shop_columns' => 5 ) ) ) ?>" class="medium-size <?php echo 5 == $columns ? 'active' : '' ?>">
								<svg viewBox="0 0 15 15">
									<use xlink:href="#medium-view-size"></use>
								</svg>
							</a>
							<a href="<?php echo esc_url( add_query_arg( array( 'shop_columns' => 4 ) ) ) ?>" class="large-size <?php echo 4 == $columns ? 'active' : '' ?>">
								<svg viewBox="0 0 15 15">
									<use xlink:href="#large-view-size"></use>
								</svg>
							</a>
						</li>
						<?php if ( is_active_sidebar( 'shop-filter' ) && sober_get_option( 'products_filter' ) ) : ?>
							<li class="data filter">
								<a href="#" class="toggle-filter">
									<svg viewBox="0 0 20 20">
										<use xlink:href="#filter"></use>
									</svg>
									<?php esc_html_e( 'Filter', 'sober' ) ?>
								</a>

								<div class="filter-widgets woocommerce">
									<button class="close">
										<svg viewBox="0 0 20 20">
											<use xlink:href="#close-delete"></use>
										</svg>
									</button>
									<?php dynamic_sidebar( 'shop-filter' ) ?>
								</div>
							</li>
						<?php endif; ?>
					</ul>
				</div>
			</div>
		</div>

		<?php
	}

	/**
	 * Filter function for breadcrumb args
	 *
	 * @param array $args
	 *
	 * @return mixed
	 */
	public function breadcrumb_args( $args ) {
		$args['delimiter']   = '<i class="fa fa-angle-right" aria-hidden="true"></i>';
		$args['wrap_before'] = '<nav class="woocommerce-breadcrumb breadcrumb" ' . ( is_single() ? 'itemprop="breadcrumb"' : '' ) . '>';

		return $args;
	}

	/**
	 * Display product breadcrumb
	 */
	public function product_breadcrumb() {
		?>

		<div class="product-breadcrumb">
			<nav class="product-navigation post-navigation">
				<?php previous_post_link( '%link', '<svg viewBox="0 0 20 20"><use xlink:href="#left-arrow"></use></svg><span class="screen-reader-text">%title</span>' ) ?>
				<?php next_post_link( '%link', '<svg viewBox="0 0 20 20"><use xlink:href="#right-arrow"></use></svg><span class="screen-reader-text">%title</span>' ) ?>
			</nav>
			<?php woocommerce_breadcrumb() ?>
		</div>

		<?php
	}

	/**
	 * Add Bootstrap's column classes for product
	 *
	 * @since 1.0
	 *
	 * @param array  $classes
	 * @param string $class
	 * @param string $post_id
	 *
	 * @return array
	 */
	public function product_class( $classes, $class = '', $post_id = '' ) {
		if ( is_admin() && ! DOING_AJAX ) {
			return $classes;
		}

		// Add classes for product
		if (
			get_post_meta( $post_id, '_is_new', true ) ||
			( $this->new_duration && ( time() - ( 60 * 60 * 24 * $this->new_duration ) ) < strtotime( get_the_time( 'Y-m-d' ) ) )
		) {
			$classes[] = 'new';
		}

		// Add classes for products in archive page only
		if ( ! $post_id || get_post_type( $post_id ) !== 'product' || is_single( $post_id ) ) {
			return $classes;
		}
		global $woocommerce_loop, $product;

		$classes[] = 'col-md-4 col-sm-4 col-xs-6';

		if ( $woocommerce_loop['columns'] == 5 ) {
			$classes[] = 'col-lg-1-5';
		} else {
			$classes[] = 'col-lg-' . ( 12 / $woocommerce_loop['columns'] );
		}

		$gallery_image_ids = $product->get_gallery_attachment_ids();
		if ( ! empty( $gallery_image_ids ) ) {
			$classes[] = 'product-has-gallery';
		}

		return $classes;
	}

	/**
	 * Display badge for new product or featured product
	 *
	 * @since 1.0
	 */
	public function product_ribbons() {
		global $product;

		$output = array();

		if ( $product->is_on_sale() ) {
			$percentage = 0;

			if ( $product->product_type == 'variable' ) {
				$available_variations = $product->get_available_variations();
				$max_percentage       = 0;

				for ( $i = 0; $i < count( $available_variations ); $i ++ ) {
					$variation_id     = $available_variations[ $i ]['variation_id'];
					$variable_product = new WC_Product_Variation( $variation_id );
					$regular_price    = $variable_product->regular_price;
					$sales_price      = $variable_product->sale_price;
					$percentage       = $regular_price ? round( ( ( ( $regular_price - $sales_price ) / $regular_price ) * 100 ) ) : 0;

					if ( $percentage > $max_percentage ) {
						$max_percentage = $percentage;
					}
				}
			} elseif ( $product->product_type == 'simple' ) {
				$percentage = round( ( ( $product->regular_price - $product->sale_price ) / $product->regular_price ) * 100 );
			}

			if ( $percentage ) {
				$off      = sober_get_option( 'product_onsale_off' ) ? ' ' . esc_html__( 'OFF', 'sober' ) : '';
				$output[] = '<span class="onsale ribbon">' . $percentage . '%' . $off . '</span>';
			}
		}

		if ( $product->is_featured() ) {
			$output[] = '<span class="featured ribbon">' . esc_html__( 'Hot', 'sober' ) . '</span>';
		}

		if (
			get_post_meta( $product->get_id(), '_is_new', true ) ||
			( ( time() - ( 60 * 60 * 24 * $this->new_duration ) ) < strtotime( get_the_time( 'Y-m-d' ) ) )
		) {
			$output[] = '<span class="newness ribbon">' . esc_html__( 'New', 'sober' ) . '</span>';
		}

		if ( $output ) {
			printf( '<span class="ribbons">%s</span>', implode( '', $output ) );
		}
	}

	/**
	 * Open product thumbnail wrapper
	 */
	public function open_loop_thumbnail_wrapper() {
		?><div class="product-header"><?php
	}

	/**
	 * Close product thumbnail wrapper
	 */
	public function close_loop_thumbnail_wrapper() {
		?></div><?php
	}

	/**
	 * Add billing title
	 */
	public function billing_title() {
		if ( wc_ship_to_billing_address_only() && WC()->cart->needs_shipping() ) : ?>

			<h3><?php esc_html_e( 'Billing &amp; Shipping', 'sober' ); ?></h3>

		<?php else : ?>

			<h3><?php esc_html_e( 'Billing Details', 'sober' ); ?></h3>

		<?php endif;
	}

	/**
	 * Add hover image for a product on catalog page
	 */
	public function show_product_loop_hover_thumbnail() {
		global $product;

		if ( ! sober_get_option( 'product_hover_thumbnail' ) ) {
			return;
		}

		$image_ids = $product->get_gallery_attachment_ids();

		if ( empty( $image_ids ) ) {
			return;
		}

		echo wp_get_attachment_image( $image_ids[0], 'shop_catalog', false, array( 'class' => 'attachment-shop_catalog size-shop_catalog product-hover-image' ) );
	}

	/**
	 * Show product buttons inside the .product-header div
	 * This contains add_to_cart and wishlist buttons
	 */
	public function show_product_loop_buttons() {
		?>

		<div class="buttons">
			<?php
			woocommerce_template_loop_add_to_cart();

			if ( shortcode_exists( 'add_to_wishlist' ) ) {
				echo do_shortcode( '[add_to_wishlist]' );
			} elseif ( shortcode_exists( 'yith_wcwl_add_to_wishlist' ) ) {
				echo do_shortcode( '[yith_wcwl_add_to_wishlist]' );
			}

			if ( sober_get_option( 'product_quickview' ) && sober_get_option( 'product_quickview_behavior' ) == 'view_button' ) {
				printf( '<a href="%s" class="quick_view_button button"><svg width="20" height="29"><use xlink:href="#quickview-eye"></use></svg></a>', esc_url( get_permalink() ) );
			}
			?>
		</div>

		<?php
	}

	/**
	 * Print new product title shop page with link inside
	 */
	public function show_product_loop_title() {
		?>

		<h3><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h3>

		<?php
	}

	public function price_html( $price, $product ) {
		if ( ! $product->is_in_stock() ) {
			$price = esc_html__( 'Sold Out', 'sober' );
		}

		return $price;
	}

	/**
	 * Display product toolbar on single product page
	 */
	public function product_toolbar() {
		?>

		<div class="product-toolbar">
			<?php
			the_post_navigation( array(
				'screen_reader_text' => esc_html__( 'Product navigation', 'sober' ),
				'prev_text'          => _x( '<svg viewBox="0 0 20 20"><use xlink:href="#left-arrow"></use></svg><span class="screen-reader-text">%title</span>', 'Previous post link', 'sober' ),
				'next_text'          => _x( '<span class="screen-reader-text">%title</span><svg viewBox="0 0 20 20"><use xlink:href="#right-arrow"></use></svg>', 'Next post link', 'sober' ),
			) );

			if ( function_exists( 'yoast_breadcrumb' ) ) {
				yoast_breadcrumb( '<div class="breadcrumb">', '</div>' );
			} else {
				woocommerce_breadcrumb();
			}
			?>
		</div>

		<?php
	}

	/**
	 * Open product summary div
	 */
	public function open_product_summary() {
		echo '<div class="product-summary clearfix">';
	}

	/**
	 * Close product summary div
	 */
	public function close_product_summary() {
		echo '</div>';
	}

	/**
	 * Change thumbnail size in single product page
	 *
	 * @param string $size
	 *
	 * @return string
	 */
	public function small_thumbnail_size( $size ) {
		if ( 'style-1' == sober_get_option( 'single_product_style' ) ) {
			$size = 'shop_single';
		}

		return $size;
	}

	/**
	 * Prepare product images for quick view
	 */
	public function prepare_quickview_images() {
		global $product;

		$attachment_ids = $product->get_gallery_attachment_ids();
		$images         = array( get_the_post_thumbnail( null, 'shop_single' ) );


		foreach ( $attachment_ids as $attachment_id ) {
			$images[] = wp_get_attachment_image( $attachment_id, 'shop_single' );
		}

		printf( '<div id="quickview-images" class="quickview-images">%s</div>', implode( "\n\t", $images ) );
	}

	/**
	 * Open sticky product summary container
	 */
	public function open_sticky_description() {
		echo '<div class="sticky-summary">';
	}

	/**
	 * Close sticky product summary container
	 */
	public function close_sticky_description() {
		echo '</div>';
	}

	/**
	 * Add wishlist button again
	 */
	public function single_product_wishlist_button() {
		global $product;

		// Button was added to variable products manually
		if ( $product->is_type( 'variable' ) ) {
			return;
		}

		if ( shortcode_exists( 'add_to_wishlist' ) ) {
			echo do_shortcode( '[add_to_wishlist]' );
		}
	}

	/**
	 * Wrap reset variations link with a div container
	 *
	 * @param $link
	 *
	 * @return string
	 */
	public function reset_variations_link( $link ) {
		return '<div class="variations-reset">' . $link . '</div>';
	}

	/**
	 * Product share
	 * Share on Facebook, Twitter, Pinterest
	 */
	public function share() {
		$socials = sober_get_option( 'product_share' );

		if ( empty( $socials ) ) {
			return;
		}
		?>

		<div class="product-share">
			<?php if ( in_array( 'facebook', $socials ) ) : ?>

				<a href="<?php echo esc_url( add_query_arg( array( 'u' => rawurlencode( get_permalink() ) ), 'https://www.facebook.com/sharer/sharer.php' ) ) ?>" target="_blank" class="facebook-share-link">
					<i class="fa fa-facebook"></i><?php esc_html_e( 'Facebook', 'sober' ) ?>
				</a>
			<?php endif; ?>

			<?php if ( in_array( 'twitter', $socials ) ) : ?>
				<a href="<?php echo esc_url( add_query_arg( array( 'url' => rawurlencode( get_permalink() ), 'text' => rawurlencode( get_the_title() ) ), 'https://twitter.com/intent/tweet' ) ) ?>" target="_blank" class="twitter-share-link">
					<i class="fa fa-twitter"></i><?php esc_html_e( 'Twitter', 'sober' ) ?>
				</a>
			<?php endif; ?>

			<?php if ( in_array( 'pinterest', $socials ) ) : ?>
				<a href="<?php echo esc_url( add_query_arg( array( 'url' => rawurlencode( get_permalink() ), 'media' => get_the_post_thumbnail_url(), 'description' => rawurlencode( get_the_title() ) ), 'http://pinterest.com/pin/create/button' ) ) ?>" target="_blank" class="pinterest-share-link">
					<i class="fa fa-pinterest-p"></i><?php esc_html_e( 'Pinterest', 'sober' ) ?>
				</a>
			<?php endif; ?>
		</div>

		<?php
	}

	/**
	 * Add extra content at bottom of product's short description
	 */
	public function product_extra_content() {
		$content = sober_get_option( 'single_product_extra_content' );

		if ( empty( $content ) ) {
			return;
		}

		printf( '<div class="product-extra-content">%s</div>', do_shortcode( wp_kses_post( $content ) ) );
	}

	/**
	 * Change product tab titles
	 * Add <span> to the counter beside "Review" tab
	 *
	 * @param array $tabs
	 *
	 * @return array
	 */
	public function product_tabs( $tabs ) {
		foreach ( $tabs as &$tab ) {
			$tab['title'] = str_replace( array( '(', ')' ), array(
				'<span class="counter">',
				'</span>',
			), $tab['title'] );
		}

		return $tabs;
	}

	/**
	 * Change related & up-sell products columns
	 *
	 * @param int $columns
	 *
	 * @return int
	 */
	public function related_products_columns( $columns ) {
		global $woocommerce_loop;

		if ( 'style-3' == sober_get_option( 'single_product_style' ) ) {
			if ( isset( $woocommerce_loop['name'] ) && $woocommerce_loop['name'] == 'up-sells' ) {
				$columns = 2;
			} else {
				$columns = 4;
			}
		} else {
			$columns = 5;
		}

		return $columns;
	}

	/**
	 * Change cross sell product columns
	 *
	 * @return int
	 */
	public function cross_sell_columns() {
		return 4;
	}
}
