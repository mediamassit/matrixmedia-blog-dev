<?php
/**
 * Single Product Image
 *
 * This template is overridden to remove prettyPhotos effect
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see           https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package       WooCommerce/Templates
 * @version       2.6.3
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $post, $product;
?>
<div class="images">
	<?php
	if ( has_post_thumbnail() ) {
		$attachment_count = count( $product->get_gallery_attachment_ids() );
		$gallery          = $attachment_count > 0 ? '[product-gallery]' : '';
		$original         = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
		$props            = wc_get_product_attachment_props( get_post_thumbnail_id(), $post );
		$image            = get_the_post_thumbnail( $post->ID, apply_filters( 'single_product_large_thumbnail_size', 'shop_single' ), array(
			'title' => $props['title'],
			'alt'   => $props['alt'],
		) );
		echo apply_filters(
			'woocommerce_single_product_image_html',
			sprintf(
				'<a href="%s" itemprop="image" class="woocommerce-main-image photoswipe" title="%s" data-width="%s" data-height="%s">%s</a>',
				esc_url( $props['url'] ),
				esc_attr( $props['caption'] ),
				esc_attr( $original[1] ),
				esc_attr( $original[2] ),
				$image
			),
			$post->ID
		);
	} else {
		echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<img src="%s" alt="%s" />', wc_placeholder_img_src(), __( 'Placeholder', 'sober' ) ), $post->ID );
	}

	do_action( 'woocommerce_product_thumbnails' );
	?>
</div>
