<?php
/**
 * Template part for displaying header with center menu.
 *
 * @package Sober
 */
?>

<div class="row">
	<div class="mobile-nav-toggle col-xs-3 col-sm-3 col-md-3 hidden-lg">
		<span class="toggle-nav" data-target="mobile-menu"><span class="icon-nav"></span></span>
	</div>

	<div class="site-branding col-xs-6 col-sm-6 col-md-6 col-lg-3">
		<?php get_template_part( 'template-parts/logo' ); ?>
	</div><!-- .site-branding -->

	<nav id="site-navigation" class="main-navigation site-navigation hidden-xs hidden-sm hidden-md col-lg-6">
		<?php wp_nav_menu( array( 'theme_location' => 'primary', 'container' => false, 'menu_class' => 'nav-menu' ) ); ?>
	</nav><!-- #site-navigation -->

	<div class="header-icon col-xs-3 col-sm-3 col-md-3 col-lg-3">
		<ul class="hidden-xs hidden-sm hidden-md">
			<li class="menu-item menu-item-search">
				<a href="#" data-toggle="modal" data-target="search-modal">
					<svg viewBox="0 0 20 20">
						<use xlink:href="#search"></use>
					</svg>
				</a>
			</li>
			<?php if ( function_exists( 'WC' ) ) : ?>
				<li class="menu-item menu-item-account">
					<a href="<?php echo wc_get_account_endpoint_url( 'dashboard' ) ?>" data-toggle="<?php echo is_user_logged_in() ? 'link' : 'modal' ?>" data-target="login-modal">
						<svg viewBox="0 0 20 20">
							<use xlink:href="#user-account-people"></use>
						</svg>
					</a>
				</li>
				<li class="menu-item menu-item-cart">
					<a href="<?php echo esc_url( wc_get_cart_url() ) ?>" class="cart-contents" data-toggle="modal" data-target="cart-modal" data-tab="cart">
						<svg viewBox="0 0 20 20">
							<use xlink:href="#basket-addtocart"></use>
						</svg>
						<span class="count cart-counter"><?php echo intval( WC()->cart->get_cart_contents_count() ) ?></span>
					</a>
				</li>
			<?php endif; ?>
		</ul>

		<?php if ( function_exists( 'WC' ) ) : ?>
			<a href="<?php echo esc_url( wc_get_cart_url() ) ?>" class="cart-contents menu-item-mobile-cart hidden-lg" data-toggle="modal" data-target="cart-modal" data-tab="cart">
				<svg viewBox="0 0 20 20">
					<use xlink:href="#basket-addtocart"></use>
				</svg>
				<?php if ( sober_get_option( 'mobile_cart_badge' ) ) : ?>
					<span class="count cart-counter"><?php echo intval( WC()->cart->get_cart_contents_count() ) ?></span>
				<?php endif; ?>
			</a>
		<?php endif; ?>
	</div><!-- .header-icon -->
</div>