=== Sober ===

Author: uixthemes
Tags: translation-ready, theme-options, custom-menu, post-formats, threaded-comments

Requires at least: 4.1
Tested up to: 4.7.2
Stable tag: 1.1.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Sober is a fully responsive Premium WordPress Theme with a pixel perfect design and extensive functionality.

== Description ==

Sober is an unique and modern WordPress ecommerce theme built with Bootstrap and powered by Visual Composer. It was built for your digital store, hitech store, watch store, men store, women store, clothing store, furniture store, book store, cosmetics shop, luxury jewelry and accessories store...

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Changelog ==

Version 1.1.2
* Add a new option to select menu hover animation
* Add a new option to show/hide blog category list
* Add a new option to manage post header fields
* Add a new option to show product buttons on mobile
* Improve Yoast SEO compatible
* Tweak author bio box: add link to author posts
* Fix bug ajax load more products without adding CSS column classes
* Fix bug shop tab filter by tags doesn't work
* Fix a small CSS issue on checkout page
* Fix the javascript error of page header parallax
* Fix issue when has no page header on blog page

Version 1.1.1
* Add new option for page header parallax
* Add new options to manage product filter tab on shop page
* Add new option to hide price for "Out of stock" products and show "Sold Out"
* Add new option to add close icon to mobile menu
* Update style for grouped products
* Update FontAwesome to version 4.7.0
* Fix bug sticky product description on product style 1 doesn't work on Safari sometimes
* Fix bug slide menu doesn't work with sticky header V6
* Fix bug product doesn't show image on mobile when use product style 1
* Fix CSS issue with variation swatches
* Fix wrong price direction
* Fix warning "Divide by zero"
* Fix partner images are cropped (Please update Sober Addons plugin to have this fix)

Version 1.1.0
* New - Support portfolio
* New - Support product variation color swatches with plugin "WooCommerce Variation Swatches"
* Add new option that allow to add extra content at bottom of product short description on every product page
* Add new option to control product sharing
* Add new option to add more content on the right side of footer social icons
* Add new option that allows add product number badge next to shopping cart icon on mobile menu
* Add new option to change mobile menu width
* Improve loading performance and help browser render the webpage a litle bit faster
* Fix CSS issue for pages which has no content
* Fix bug about responsive
* Fix warning when a product has no price

Version 1.0.8
* Improvement performance
* Improvement shop cart display on mobile
* Fix issue about sticky header style
* Fix issue modal can't scroll
* Fix product images are not showed on mobile when quick-view is off

Version 1.0.7
* Add infinity scroll for shop page
* Improve user experience on mobile for product list and product style 1
* Fix mega menu settings lost data when save

Version 1.0.6
* Update Sober Addons plugin to version 1.1.1
* Update Soo Wishlist plugin to version 1.0.1
* Update Soo Product Filter to version 1.0.1
* New option to use text logo
* New option to disable header hover effect
* New option to change shop tabs from "Hot, News, Sales" to top 3 categories
* New option to add "OFF" word to sale badge, after percentage number
* Fix bug logo size can not be applied
* Fix bug logo margin can not be applied
* Fix normal sticky header style on Cart, Wishlist, Order Tracking pages


Version 1.0.5
* Update Sober Addons plugin to version 1.1
* Start supporting Yoast SEO breadcrumb
* Fix quick view modal responsive problem
* Fix banner images don't fill grid on iMac screen
* Fix sticky header style on pages which have no page header
* Fix product variation dropdown style on Mac OS

Version 1.0.4
* Improve performance by reducing site size
* Press ESC button to close modal
* Add sticky header feature

Version 1.0.3
* Improve responsive display on mobile
* Improve performance
* Fix sticky product summary problem on Safari
* Fix dropdown style problem on Safari
* Fix search icon in mobile menu

Version 1.0.2
* Use SVG for default logo
* Improve mobile menu performance
* Fix error when WooCommerce plugin is not activated

Version 1.0.1
* Add product option that allow to set a product as new product
* Add configuration file for supporting WPML
* Update recommended plugin list
* Fix some mirror issues in CSS

Version 1.0
* Initial release

== Credits ==

* Based on Underscores http://underscores.me/, (C) 2012-2016 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
* Bootstrap http://getbootstrap.com/, Code licensed MIT, docs CC BY 3.0
