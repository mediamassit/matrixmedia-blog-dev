<?php

if(function_exists('acf_add_options_page')) {
	$page = acf_add_options_page(apply_filters('mm/acf/add-options-page/mm-website', array(
		'page_title' => __( 'Ustawienia witryny', 'mm-plugin' ),
		'menu_title' => __( 'Witryna', 'mm-plugin' ),
		'menu_slug' => 'mm-website',
		'capability' => 'edit_posts',
		'icon_url' => 'dashicons-editor-paste-word',
		'position' => 50,
		'redirect' => false
	)));
}
//

