<?php

function mm_taxonomy_realization() {
	$labels = array(
		'name'              => _x( 'Kategorie oferty', '' ),
		'singular_name'     => _x( 'Kategoria oferty', '' ),
		'search_items'      => __( 'Szukaj' ),
		'all_items'         => __( 'Wszystkie oferty' ),
		'parent_item'       => __( 'Rodzic' ),
		'parent_item_colon' => __( 'Rodzic:' ),
		'edit_item'         => __( 'Edytuj' ),
		'update_item'       => __( 'Zaaktualizuj' ),
		'add_new_item'      => __( 'Dodaj nową ofertę' ),
		'new_item_name'     => __( 'Nowa nazwa' ),
		'menu_name'         => __( 'Kategorie' ),
	);
	$rewrite = array(
		'slug'                       => 'kategoria-oferty',
		'with_front'                 => true,
		'hierarchical'               => false,
	);
	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => $rewrite
	);
	register_taxonomy( 'offert_cat',  array('offert'), $args );
}
add_action( 'init', 'mm_taxonomy_realization', 0 );